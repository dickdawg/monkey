# Monkey HTTP Server
## Current Version **1.6**

# Quick Build
### ‘Local’ Install

```
tar zxfv monkey-1.6.0.tar.gz
cd monkey-1.6.0
./configure —local
make
```

Builds complete functioning web server in current directory location. 

If a traditional install and file layout is desired - - -skip to….

# Custom Build
```
$ ./configure -h
Usage: ./configure [OPTION]… [VAR=VALUE]…

Optional Commands:
```

### Build options:
  **—local**                 Build locally, do not install (dev mode)
  **—debug**                 Compile Monkey with debugging symbols
  **—trace**                 Enable trace messages (do not use in production)
  **—no-backtrace**          Disable backtrace feature
  **—linux-trace**           Enable Linux Trace Toolkit
  **—musl-mode**             Enable musl compatibility mode
  **—uclib-mode**            Enable uClib compatibility mode
  **—malloc-libc**           Use system default memory allocator (default is jemalloc)
  **--pthread-tls**           Use Posix thread keys instead of compiler TLS
  **—no-binary**             Do not build binary
  **—static-lib-mode**       Build static library mode
  **—skip-config**           Do not include configuration files

### Installation Directories:
  **—prefix=PREFIX**         Root prefix directory
  **—sbindir=BINDIR**        Binary files (executables)
  **—libdir=LIBDIR**         Libraries
  **—includedir=INCDIR**     Header install path
  **—sysconfdir=SYSCONFDIR** Configuration files
  **—webroot=WEB_ROOT**      Path to default web site files
  **—mandir=MANDIR**         Manpages - documentation
  **—logdir=LOGDIR**         Log files
  **—pidfile=PIDFILE**       Path to file to store PID
  **—systemddir**[=DIR]      Systemd directory path
  **—enable-plugins=**a,b    Enable the listed plugins
  **—disable-plugins=**a,b   Disable the listed plugins
  **—static-plugins=**a,b    Build plugins in static mode
  **—only-accept**           Use only accept(2)
  **—only-accept4**          Use only accept4(2) (default and preferred)

### Override Server Configuration:
  **—default-port=PORT**     Override default TCP port (default: 2001)
  **—default-user=USER**     Override default web user (default: www-data)

```
./configure --sbindir=/usr/bin/ --prefix=/home/monkey --sysconfdir=/home/monkey/configuration/  --pidfile=/home/monkey/logs/pid --default-user=monkey --mandir=/home/monkey/help --enable-plugins=tls
```
